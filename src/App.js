import "./App.css";
import HelloWord from "./HelloWord.jsx";

function App() {
  return (
    <div className="App">
      <HelloWord />
    </div>
  );
}

export default App;
